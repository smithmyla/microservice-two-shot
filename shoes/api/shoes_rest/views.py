from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import BinVO, Shoe

# Create your views here.
class BinVOEncoder(ModelEncoder):
     model = BinVO
     properties = ["closet_name", "import_href"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "id",
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
     model = Shoe
     properties = [
          "model_name",
          "manufacturer",
          "color",
          "picture_url",
          "bin",
          "id",
          ]

     encoders = {
         "bin": BinVOEncoder(),
     }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else: #POST
        content = json.loads(request.body)
        try:
            bin_href = f'/api/bins/{content["bin"]}/'
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Bruh"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
     if request.method == "GET":
          shoe = Shoe.objects.get(pk=pk)
          return JsonResponse(
               shoe,
               encoder=ShoeDetailEncoder,
               safe=False,
          )
     elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
     else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(import_href=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin pk"},
                status=400,
            )
        Shoe.objects.filter(pk=pk).update(**content)
        shoe = Shoe.objects.get(pk=pk)
        return JsonResponse(
            Shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
