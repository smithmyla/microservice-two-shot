import React, { useEffect, useState, } from 'react';

function ShoesList() {
    const [shoes, setShoes] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
    }
  }
  useEffect(() => {
      fetchData();
  }, []);

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Model Name</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{ shoe.model_name }</td>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.color}</td>
                <td><img src={shoe.picture_url} alt="Shoe"></img></td>
                <td>{ shoe.bin }</td>

              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoesList;
