import React, { useEffect, useState, } from 'react';

function HatsList() {

    const [hats, setHats] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }
    useEffect (() => {
        fetchData();
    },[]);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.fabric}</td>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                            <td><img src={hat.picture_url} alt="Hat"></img></td>
                            <td>{hat.location.closet_name}</td>
                        </tr>
                    );

                })}
            </tbody>
        </table>
    );
}

export default HatsList;
